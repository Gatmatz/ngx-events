import { createSelector } from '@ngrx/store';
import { EventsComponentActions, EventsComponentActionTypes } from '../actions/index';
import { TableSrcConfiguration, CalendarConfiguration } from '../components/events/events.component'
import { merge } from 'lodash';

export interface EventsComponentState {
    view: string,
    calendar?: CalendarConfiguration,
    table?: {
        active: TableSrcConfiguration,
        sources: Array<TableSrcConfiguration>
    }
}

export interface EventsState {
    eventsComponent: EventsComponentState;
}

export const featureName = 'events';
export const initialState: EventsState = {
    eventsComponent: {
        view: 'table'
    }
}

export function EventReducer(state = initialState, action: EventsComponentActions): EventsState {
    switch (action.type) {
        case EventsComponentActionTypes.SetView:
            return merge({}, state, {
                eventsComponent: {
                    view: action.payload
                }
            });
        case EventsComponentActionTypes.SetTable:
            return merge({}, state, {
                eventsComponent: {
                    table: action.payload
                }
            });
        case EventsComponentActionTypes.SetCalendar:
            return merge({}, state, {
                eventsComponent: {
                    calendar: action.payload
                }
            });
        default:
            return state;
    }
}

export const selectEvents = (state: any) => state.events;
export const selectEventsComponent = (state: any) => state.eventsComponent;

export const EventsComponentSelectors = {
    view: createSelector(
        selectEvents,
        selectEventsComponent,
        (state: EventsComponentState) => state.view
    ),
    table: createSelector(
        selectEvents,
        selectEventsComponent,
        (state: EventsComponentState) => state.table
    ),
    calendar: createSelector(
        selectEvents,
        selectEventsComponent,
        (state: EventsComponentState) => state.calendar
    ),
    all: createSelector(
        selectEvents,
        selectEventsComponent
    )
}
