import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AppEventService } from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable-dashboard',
  templateUrl: './timetable-dashboard.component.html'
})
export class TimetableDashboardComponent implements OnInit, OnDestroy {

  public model: any;
  public tabs: any[];
  private maxExtraTabs = 3;
  private fixedTabs= [];
  private eventsPreviewed = [];
  private paramSubscription: Subscription;
  private changeSubscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
  ) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.model = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .expand('availableEventTypes')
        .getItem();

      this.model.eventTypes = this.model.availableEventTypes.map(type => type.alternateName);
      this.fixedTabs = this._activatedRoute.routeConfig.children.filter(route => {
        if (typeof route.redirectTo === 'undefined' && route.data.title) {
          if (route.data.model === 'TeachingEvent' || route.data.model === 'ExamEvent') {
            return this.model.eventTypes.includes(route.data.model);
          }
          return true;
        }
      });
      this.tabs = [...this.fixedTabs];
      this.eventsPreviewed = [];
    });

    // add extra tabs for previewing events and organize them in a first-in-first-out manner
    this.changeSubscription = this._appEvent.changed.subscribe(async ({ target, action, model } = {}) => {
      if (this.model && target && model && this.model.eventTypes.includes(model)) {
        const eventIndex = this.eventsPreviewed.findIndex(event => String(event.id) === String(target.id));

        if (eventIndex === -1 && action === 'preview') {
          // add event at the beginning of the previewed events
          this.eventsPreviewed.unshift(target);

          // if the allowed number of previewed events is exceeded, remove the last one
          if (this.eventsPreviewed.length > this.maxExtraTabs) {
            this.eventsPreviewed.pop();
          }
        } else if (action === 'remove') {
          if (eventIndex >= 0) {
            // remove the tab associated with the deleted event
            this.eventsPreviewed.splice(eventIndex, 1);
          } else {
            // remove any tabs associated with the sub-events of the deleted event
            this.eventsPreviewed = this.eventsPreviewed.filter(event => String(event.superEvent) !== String(target.id))
          }
        }

        // merge fixed tabs with the additional tabs
        this.tabs = [
          ...this.fixedTabs,
          ...this.eventsPreviewed.map(({ name, id }) => ({
            data: { title: name },
            path: `events/${id}`
          }))
        ];
      }
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
