import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService } from '@universis/common';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-timetable-dashboard-overview-actions',
  templateUrl: './timetable-dashboard-overview-actions.component.html'
})
export class TimetableDashboardOverviewActionsComponent implements OnInit, OnChanges, OnDestroy {

  @Input() event;
  public actions: any;
  public errorMessage;
  private modalRef: BsModalRef;
  private changeSubscription: Subscription;

  constructor(
    private _translate: TranslateService,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
    private _modalService: BsModalService
    ) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes.event) {
      if (changes.event.currentValue == null) {
        this.actions = null;
        return;
      }
      this.actions = await this.loadActions();
    }
  }

  ngOnInit() {
    this.changeSubscription = this._appEvent.changed.subscribe(async (event) => {
      if (event && event.model === 'CopyTimetableEventActions') {
        this.actions = await this.loadActions();
      }
    });
  }

  async loadActions() {
    return this._context.model('CopyTimetableEventActions')
      .where('timetableEvent').equal(this.event.id)
      .expand('createdBy', 'actionStatus', 'result')
      .orderByDescending('dateCreated')
      .prepare()
      .take(-1)
      .getItems();
  }

  showFailures(modal: TemplateRef<any>, action) {
    this.errorMessage = action.failures;
    this.modalRef = this._modalService.show(modal, {
      class: 'modal-xl'
    });
  }

  hideFailures() {
    this.modalRef.hide();
  }

  ngOnDestroy(): void {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
