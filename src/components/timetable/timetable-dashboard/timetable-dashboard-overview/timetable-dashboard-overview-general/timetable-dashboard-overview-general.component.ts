import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timetable-dashboard-overview-general',
  templateUrl: './timetable-dashboard-overview-general.component.html'
})
export class TimetableDashboardOverviewGeneralComponent implements OnInit {

  @Input() event;

  constructor() { }

  ngOnInit() {
  }

}
