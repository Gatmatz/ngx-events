export const ABSENCES_CONFIGURATION = {
    "title": "StudentPeriodRegistrations.Courses",
    "model": "StudentCourseClasses",
    "searchExpression":"indexof(courseClass/title, '${text}') ge 0 or indexof(courseClass/course/displayCode, '${text}') ge 0 ",
    "selectable": true,
    "multipleSelect": true,
    "columns": [
      {
        "name": "id",
        "formatter": "ButtonFormatter",
        "property": "id",
        "className": "text-center",
        "formatOptions": {
          "buttonContent": "<i class=\"fas fa-edit text-indigo\"></i>",
          "buttonClass": "btn btn-default",
          "commands": [
            { "outlets":
            {
              "modal":["item", "${id}", "edit"]
            }
            }
          ],
          "navigationExtras": {
            "replaceUrl": false,
            "skipLocationChange": true
          }
        }
      },
      
      {
        "name":"student/familyName",
        "property": "familyName",
        "title":"student"
      },
      {
        "name":"studentCourseClass",
        "property": "studentCourseClass",
        "title":"studentCourseClass"
      },
      {
        "name":"attendances",
        "title":"attendances"
      }
    ],
    // "defaults":{
    //   "orderBy": "semester asc,courseClass/title asc"
    // },
  //  "paths": [
  
    // ],
    // "criteria" : [
    //   {
    //     "name": "semester",
    //     "filter": "(semester/name eq '${value}')",
    //     "type": "text"
    //   },
    //   {
    //     "name": "name",
    //     "filter": "(indexof(courseClass/title, '${value}') ge 0)",
    //     "type": "text"
    //   },
    //   {
    //     "name": "displayCode",
    //     "filter": "(indexof(course/displayCode, '${value}') ge 0)",
    //     "type": "text"
    //   },
    //   {
    //     "name": "courseSector",
    //     "filter": "(course/courseSector eq '${value}')",
    //     "type": "text"
    //   },
    //   {
    //     "name": "courseTypeName",
    //     "filter": "(course/courseType/course eq '${value}')",
    //     "type": "text"
    //   }
    //]
  }
  